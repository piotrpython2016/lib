package com.library.lib.service;

import com.library.lib.mapper.BookMapper;
import com.library.lib.model.Author;
import com.library.lib.model.Book;
import com.library.lib.repo.AuthorRepository;
import com.library.lib.repo.BookRepository;
import com.library.lib.repo.RentalRepository;
import com.library.lib.web.BookDTO;
import com.library.lib.web.BookResponse;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class BookService {
    private final BookRepository bookRepository;
    private final BookMapper bookMapper;
    private final AuthorRepository authorRepository;
    private final RentalRepository rentalRepository;

    public BookService(BookRepository bookRepository, BookMapper bookMapper, AuthorRepository authorRepository, RentalRepository rentalRepository) {
        this.bookRepository = bookRepository;
        this.bookMapper = bookMapper;
        this.authorRepository = authorRepository;
        this.rentalRepository = rentalRepository;
    }

    public List<Book> displayAllAuthors() {
        return bookRepository.findAll();
    }
    public Book findBook(Long id) {
        return bookRepository.findById(id);
    }

    public void add(BookDTO BookDto) {
        Book book = bookMapper.createNew(BookDto);
        Author author = book.getAuthors();
        author.getBooks().add(book);

        bookRepository.save(book);
        if(book.getRental() != null) {
            rentalRepository.save(book.getRental());
        }

        authorRepository.save(author);
    }


    public List<BookResponse> getAllBooks() {
        return bookRepository.findAll().stream()
                .map(bookMapper::map)
                .collect(Collectors.toList());
    }

    public BookResponse getOne(Long id) {
        Book book = bookRepository.findById(id);
        return bookMapper.map(book);
    }

//    public BookResponse getByTitle(String title){
//        Book book = bookRepository.findByTitle(title);
//    }

    public void deleteBook(Long id) {
        Book book = bookRepository.findById(id);
        book.getRental().getUser().getRentals().remove(book.getRental());
        book.getRental().setUser(null);
        book.getRental().setBook(null);
        book.setRental(null);
        bookRepository.delete(book);
    }
}
