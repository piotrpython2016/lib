package com.library.lib.web;

public class BookResponse {

    private Long id;
    private String title;
    private boolean isBorrowed;
    private boolean isDamaged;
    private Long authorsId;
    private Long rentalId;

    public BookResponse(Long id, String title, boolean isBorrowed, boolean isDamaged) {
        this.id = id;
        this.title = title;
        this.isBorrowed = isBorrowed;
        this.isDamaged = isDamaged;
    }

    public void setAuthorsId(Long authorsId) {
        this.authorsId = authorsId;
    }

    public Long getRentalId() {
        return rentalId;
    }

    public void setRentalId(Long rentalId) {
        this.rentalId = rentalId;
    }

    public Long getAuthorsId() {
        return authorsId;
    }

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public boolean isBorrowed() {
        return isBorrowed;
    }

    public boolean isDamaged() {
        return isDamaged;
    }

}
