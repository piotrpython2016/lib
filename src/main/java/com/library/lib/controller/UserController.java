package com.library.lib.controller;

import com.library.lib.model.User;
import com.library.lib.message.Response;
import com.library.lib.repo.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("*")
@RestController
public class UserController {

    @Autowired
    UsersRepository repository;

    @RequestMapping(value = "/postuser", method = RequestMethod.POST)
    public void postUser(@RequestBody User user) { //postConsumer before

        repository.save(new User(user.getFirstName(), user.getLastName(), user.getTelephone() ));
    }

    @GetMapping("/findall")
    public Response findAll() {

        Iterable<User> users = repository.findAll();

        return new Response("Done", users);
    }

    @GetMapping("/user/{id}")
    public Response findUserById(@PathVariable("id") long id) {

        User user = repository.findOne(id);

        return new Response("Done", user);
    }

    @GetMapping("/findbylastname")
    public Response findByLastName(@RequestParam("lastName") String lastName) {

        List<User> user = repository.findByLastName(lastName);

        return new Response("Done", user);
    }
}

