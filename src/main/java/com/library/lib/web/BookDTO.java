package com.library.lib.web;

public class BookDTO {
    private Long id;
    private String title;
    private String details;
    private boolean isBorrowed;
    private boolean isDamaged;
    private Long authorsId;
    private Long rentalId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public boolean isBorrowed() {
        return isBorrowed;
    }

    public void setBorrowed(boolean borrowed) {
        isBorrowed = borrowed;
    }

    public boolean isDamaged() {
        return isDamaged;
    }

    public void setDamaged(boolean damaged) {
        isDamaged = damaged;
    }

    public Long getAuthorsId() {
        return authorsId;
    }

    public void setAuthorsId(Long authorsId) {
        this.authorsId = authorsId;
    }

    public Long getRentalId() {
        return rentalId;
    }

    public void setRentalId(Long rentalId) {
        this.rentalId = rentalId;
    }

    public BookDTO() {
    }

    public BookDTO(Long id, String title, String details, boolean isBorrowed, boolean isDamaged, Long authorsId, Long rentalId) {
        this.id = id;
        this.title = title;
        this.details = details;
        this.isBorrowed = isBorrowed;
        this.isDamaged = isDamaged;
        this.authorsId = authorsId;
        this.rentalId = rentalId;
    }
}
