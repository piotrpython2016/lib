package com.library.lib.mapper;

import com.library.lib.model.Book;
import com.library.lib.repo.AuthorRepository;
import com.library.lib.service.AuthorService;
import com.library.lib.service.RentalService;
import com.library.lib.web.BookDTO;
import com.library.lib.web.BookResponse;
import org.springframework.stereotype.Component;

@Component
public class BookMapper {
    private final AuthorRepository authorRepository;
    private final AuthorService authorService;
    private final RentalService rentalService;


    public BookMapper(AuthorRepository authorRepository, AuthorService authorService, RentalService rentalService) {
        this.authorRepository = authorRepository;
        this.authorService = authorService;
        this.rentalService = rentalService;
    }

    public Book createNew(BookDTO createBookDto) {
        Book book = new Book();
        book.setBorrowed(createBookDto.isBorrowed());
        book.setTitle(createBookDto.getTitle());
        book.setAuthors(authorService.findAuthor(createBookDto.getAuthorsId()));
        book.setRental(rentalService.findRental(createBookDto.getRentalId()));
        return book;
    }

    public BookResponse map(Book book) {

        BookResponse bookResponse = new BookResponse(book.getId(), book.getTitle(), book.isBorrowed(), book.isDamaged());
        if(book.isDamaged() ==false)
        bookResponse.setAuthorsId(book.getAuthors().getId());
        bookResponse.getTitle();
        if(book.getRental() != null)
            bookResponse.setRentalId(book.getRental().getId());

        return bookResponse;
    }
}
